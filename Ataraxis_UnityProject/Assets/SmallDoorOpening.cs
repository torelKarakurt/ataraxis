﻿using UnityEngine;
using System.Collections;

public class SmallDoorOpening : MonoBehaviour {
		
	public bool canOpen = true;
	
	public enum DoorState {opening, closing, open, closed}
	public DoorState dState = DoorState.closed;
	public bool isPlayerInZone = false;
	
	public GameObject topDoor;
	public GameObject topMidDoor;
	public GameObject bottomMidDoor;
	public GameObject pentagonDoor;
	public GameObject bottomDoor;

	public float doorSpeed = 3.0f;

	Vector3 originTop;
	Vector3 originTM;
	Vector3 originBM;
	Vector3 originPentagon;
	Vector3 originBottom;

	void Start(){
		originTop = topDoor.transform.position;
		originTM = topMidDoor.transform.position;
		originBM = bottomMidDoor.transform.position;
		originPentagon = pentagonDoor.transform.position;
		originBottom = bottomDoor.transform.position;
	}

	void Update(){
		if (canOpen) {
			if (isPlayerInZone) {
				if (!(Vector3.Distance (pentagonDoor.transform.position, originPentagon) >= 1.25f)) {
					//move away from origin
					SeparateDoorComponents ();
				}
			} else if (!isPlayerInZone) {
				if (Vector3.Distance (pentagonDoor.transform.position, originPentagon) > 0.01f) {
					AttachDoorComponents ();
					if (pentagonDoor.transform.position == originPentagon){
						//canOpen = false;
					}
				}
			}
		}
	}
	
	void OnTriggerEnter(Collider col){
		//while player is in the zone open the door. Else close the door.
		if (col.tag == "Player") {
			//print ("C: enter");
			isPlayerInZone = true;
		}
	}
	
	void OnTriggerExit(Collider col){
		if (col.tag == "Player") {
			//print ("C: exit");
			isPlayerInZone = false;
		}
	}
	void SeparateDoorComponents(){
		pentagonDoor.transform.Translate (Vector3.right * doorSpeed * Time.deltaTime);
		topMidDoor.transform.Translate (Vector3.right * doorSpeed * Time.deltaTime);
		bottomMidDoor.transform.Translate (Vector3.right * doorSpeed * Time.deltaTime);
		topDoor.transform.Translate ((Vector3.right + Vector3.up) * doorSpeed * Time.deltaTime);
		bottomDoor.transform.Translate ((Vector3.right + Vector3.down) * doorSpeed * Time.deltaTime);

	}
	
	void AttachDoorComponents(){
		pentagonDoor.transform.position = Vector3.MoveTowards(pentagonDoor.transform.position, originPentagon, (doorSpeed * Time.deltaTime));
		topMidDoor.transform.position = Vector3.MoveTowards(topMidDoor.transform.position, originTM, (doorSpeed * Time.deltaTime));
		bottomMidDoor.transform.position = Vector3.MoveTowards(bottomMidDoor.transform.position, originBM, (doorSpeed * Time.deltaTime));
		topDoor.transform.position = Vector3.MoveTowards(topDoor.transform.position, originTop, (doorSpeed * Time.deltaTime * 1.5f));
		bottomDoor.transform.position = Vector3.MoveTowards(bottomDoor.transform.position, originBottom, (doorSpeed * Time.deltaTime * 1.5f));

	}

}
