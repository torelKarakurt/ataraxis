﻿using UnityEngine;
using System.Collections;

public class NavScript : MonoBehaviour {

	public GameObject target;
	public bool isPlayerWatching = false;
	// Use this for initialization
	void Start () {
	}

	// Update is called once per frame
	void FixedUpdate () {

		if (IsVisibleFrom (GetComponent<Renderer>(), Camera.main)) {
			//raycast to player
			RaycastHit hit;
			if (Physics.Raycast(Camera.main.transform.position, (transform.position - Camera.main.transform.position), out hit)){
				//print (hit.transform.name);
				if (hit.transform.tag == "Enemy"){
					isPlayerWatching = true;
				}
				else {
					isPlayerWatching = false;
				}
				//if the eye has passed its open pos and it has previously closed then it must have finished blinking.
			}


		} else {
			isPlayerWatching = false;
		}




		if (isPlayerWatching) {
			GetComponent<NavMeshAgent> ().enabled = false;
		} else {
			GetComponent<NavMeshAgent> ().enabled = true;
			GetComponent<NavMeshAgent> ().destination = target.transform.position;
		}
	}

	public bool IsVisibleFrom(Renderer renderer, Camera camera){
		Plane[] planes = GeometryUtility.CalculateFrustumPlanes(camera);
		return GeometryUtility.TestPlanesAABB(planes, renderer.bounds);
	}
}
