﻿using UnityEngine;
using System.Collections;

public class HandheldCameraScript : MonoBehaviour {

	public GameObject handheldCamera;
	public GameObject handheldCameraFrame;
	public GameObject firstPersonController;
	//public CharacterMotor charMot;

	void Start () {
	
	}
	void Awake () {
		//charMot = firstPersonController.GetComponent<CharacterMotor>();
	}
	void Update () {
		if (Input.GetKey(KeyCode.RightShift)){
			handheldCamera.SetActive(true);
			handheldCameraFrame.SetActive(true);
			//charMot.canControl = false;
		} else {
			handheldCamera.SetActive(false);
			handheldCameraFrame.SetActive(false);
			//charMot.canControl = true;
		}
	}
}
