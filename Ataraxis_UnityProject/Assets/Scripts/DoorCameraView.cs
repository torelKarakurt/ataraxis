﻿using UnityEngine;
using System.Collections;

public class DoorCameraView : MonoBehaviour {

	public GameObject player;
	// Use this for initialization
	void Start () {
		player = Camera.main.gameObject;
		//player = GameObject.FindGameObjectWithTag ("Player");
	}
	
	void Update () {

		// make the sphere look away from the player
		transform.LookAt (2 * transform.position - player.transform.position);
	}
}
