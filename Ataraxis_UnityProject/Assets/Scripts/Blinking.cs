using UnityEngine;
using System.Collections;
/*
 * SET UP automatic blinking, every 3-10 seconds.
 * Add variables fatigue and heart rate and meds.
 * */
public class Blinking : MonoBehaviour {
	public bool isBlinking, hasClosed;

	//Game Objects for Eyelids and the First Person Controller
	public GameObject TopEyeLid,BottomEyeLid, firstPersonCharacter;
	//speed for the blinking
	public float blinkSpeed = 2.0f;
	//Tons of floats.
	float openPosTop,closedPos,openPosBottom,blinkTimer;


	void Start () {
		isBlinking = false;
		hasClosed = false;

		openPosTop = TopEyeLid.transform.position.y;
		openPosBottom = BottomEyeLid.transform.position.y;
		closedPos = firstPersonCharacter.transform.position.y;
		blinkTimer = calculateBlinkTime();
	}

	void Update () {
		closedPos = firstPersonCharacter.transform.position.y;
		if (blinkTimer > 0) {
			blinkTimer -= Time.deltaTime;

		} else {
			isBlinking = true;
			blinkTimer = calculateBlinkTime ();
		}

		CheckBlinking ();

		if (Input.GetKey ("e")) {
			isBlinking = true;
		}
	}

	void CheckBlinking(){
		if (isBlinking) {
			//this checks if the eye is not closed and closes it
			if (TopEyeLid.transform.position.y > closedPos && hasClosed==false) { 
				TopEyeLid.transform.Translate(new Vector3 (0,0,-1) * Time.deltaTime * blinkSpeed);
			}
			else{
				//as if it is blinking and the previous if statement is false then the eye must be closed.
				hasClosed = true; 
			}
			//if the eye is not fully closed and hasClosed is true the close the eye.
			if (TopEyeLid.transform.position.y < openPosTop && hasClosed==true) { 
				TopEyeLid.transform.Translate(new Vector3(0,0,1) * Time.deltaTime * blinkSpeed);
			}
			//if the eye has passed its open pos and it has previously closed then it must have finished blinking.
			if(TopEyeLid.transform.position.y > openPosTop && hasClosed==true){ 
				hasClosed = false;
				isBlinking = false;
				blinkTimer = calculateBlinkTime();
			}
		}
	}

	float calculateBlinkTime(){
		//3-10secs;
		return Random.Range(3.0f, 10.0f);
	}
}