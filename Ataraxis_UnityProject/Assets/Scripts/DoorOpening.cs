﻿using UnityEngine;
using System.Collections;

public class DoorOpening : MonoBehaviour {

	public bool canOpen = true;

	public enum DoorState {opening, closing, open, closed}
	public DoorState dState = DoorState.closed;
	public bool isPlayerInZone = false;

	public GameObject topDoor;
	public GameObject topLeftDoor;
	public GameObject bottomLeftDoor;
	public GameObject bottomRightDoor;
	public GameObject topRightDoor;
	public GameObject centerDoor;

	public float doorSpeed = 3.0f;

	Vector3 originPosCenter;
	Vector3 originPosT;
	Vector3 originPosTL;
	Vector3 originPosBL;
	Vector3 originPosBR;
	Vector3 originPosTR;

	void Start(){
		originPosCenter = centerDoor.transform.position;
		originPosT = topDoor.transform.position;
		originPosTR = topRightDoor.transform.position;
		originPosTL = topLeftDoor.transform.position;
		originPosBR = bottomRightDoor.transform.position;
		originPosBL = bottomLeftDoor.transform.position;


	}

	void Update(){
		if (canOpen) {
			if (isPlayerInZone) {
				if (!(Vector3.Distance (centerDoor.transform.position, originPosCenter) >= 1.25f)) {
					//move away from origin
					SeparateDoorComponents ();
				}
			} else if (!isPlayerInZone) {
				if (Vector3.Distance (centerDoor.transform.position, originPosCenter) > 0.01f) {
					AttachDoorComponents ();
					if (centerDoor.transform.position == originPosCenter){
						canOpen = false;
					}
				}
			}
		}

	}

	void OnTriggerEnter(Collider col){
		//while player is in the zone open the door. Else close the door.
		if (col.tag == "Player") {
			//print ("C: enter");
			isPlayerInZone = true;
		}
	}

	void OnTriggerExit(Collider col){
		if (col.tag == "Player") {
			//print ("C: exit");
			isPlayerInZone = false;
		}
	}

	void SeparateDoorComponents(){
		centerDoor.transform.Translate (Vector3.up * doorSpeed * Time.deltaTime);
		topDoor.transform.Translate (Vector3.up * doorSpeed * Time.deltaTime);
		topLeftDoor.transform.Translate (Vector3.right * doorSpeed * Time.deltaTime);
		bottomLeftDoor.transform.Translate ((Vector3.right + Vector3.down) * doorSpeed * Time.deltaTime);
		topRightDoor.transform.Translate (Vector3.left * doorSpeed * Time.deltaTime);
		bottomRightDoor.transform.Translate ((Vector3.left + Vector3.down) * doorSpeed * Time.deltaTime);

	}

	void AttachDoorComponents(){
		centerDoor.transform.position = Vector3.MoveTowards(centerDoor.transform.position, originPosCenter, (doorSpeed * Time.deltaTime));
		topDoor.transform.position = Vector3.MoveTowards(topDoor.transform.position, originPosT, (doorSpeed * Time.deltaTime));
		topLeftDoor.transform.position = Vector3.MoveTowards(topLeftDoor.transform.position, originPosTL, (doorSpeed * Time.deltaTime));
		bottomLeftDoor.transform.position = Vector3.MoveTowards(bottomLeftDoor.transform.position, originPosBL, (doorSpeed * Time.deltaTime * 1.5f));
		topRightDoor.transform.position = Vector3.MoveTowards(topRightDoor.transform.position, originPosTR, (doorSpeed * Time.deltaTime));
		bottomRightDoor.transform.position = Vector3.MoveTowards(bottomRightDoor.transform.position, originPosBR, (doorSpeed * Time.deltaTime * 1.5f));

	}
}
