﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class terminalUI : MonoBehaviour {

	public GameObject player;
	public GameObject canvas;
	public Image camPanelImage;

	private bool cctvOn = false;

	public Text outputText1;
	public Text outputText2;
	public Text inputText;
	private string textInput = "";
	public DoorOpening openingScript;
	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player");
		camPanelImage = canvas.transform.Find ("CamPanel").GetComponent<Image> () as Image;
	}

	void CheckText(){
		if (textInput == "help") {
			outputText1.text += "\n>\"open\"\n~opens the door\n>\"alert\"\n~contacts security\n>\"log\"\n~reads log\n>\"cctv\"\n~toggles cctv";
			outputText2.text += "\n>\"open\"\n~opens the door\n>\"alert\"\n~contacts security\n>\"log\"\n~reads log\n>\"cctv\"\n~toggles cctv";

		}
		if (textInput == "open"){
			openingScript.canOpen = true;
		}
		if (textInput == "alert"){
			outputText1.text = "\n>~contacting security~";
			outputText2.text = "\n>~contacting security~";
		}
		if (textInput == "ataraxis"){
			outputText1.text += "\n>best damn ship in the galaxy";
			outputText2.text += "\n>best damn ship in the galaxy";
		}
		if (textInput == "log"){
			//put in the logging here
			outputText1.text += "\n>log: 0";
			outputText2.text += "\n>log: 0";
		}
		if (textInput == "clear") {
			outputText1.text = ">clear\n>";
			outputText2.text = ">clear\n>";
		}
		if (textInput == "cctv") {
			//display other side of the door
			if (cctvOn){
				camPanelImage.color = new Color(0,0,0,1);
			} else if (!cctvOn){
				camPanelImage.color = new Color(0,0,0,0);
			}
			cctvOn = !cctvOn;
		}


		 
	}

	// Update is called once per frame
	void Update () {
		if (Vector3.Distance(transform.position, player.transform.position) <= 3.0f){
			if (Input.anyKeyDown){
				if (Input.GetKeyDown(KeyCode.Return)){
					//check the previous text, if its "open" then open.

					//new line
					inputText.text = ">";
					outputText1.text += "\n>"+textInput;
					outputText2.text += "\n>"+textInput;
					CheckText();
					textInput = "";

				} else {
					inputText.text += Input.inputString;
					textInput += Input.inputString;
				}
			}
		}
	}
}
